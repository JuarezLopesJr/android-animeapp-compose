package com.example.animeapp.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.animeapp.utils.Constants.HERO_TEST_DATABASE
import com.example.animeapp.utils.DatabaseConverter

@Database(entities = [Hero::class, HeroRemoteKeys::class], version = 1)
@TypeConverters(DatabaseConverter::class)
abstract class HeroDB : RoomDatabase() {

    /* inMemoryDatabaseBuilder for use in tests */
    companion object {
        fun create(context: Context, useInMemory: Boolean): HeroDB {
            val dataBuilder = if (useInMemory) {
                Room.inMemoryDatabaseBuilder(context, HeroDB::class.java)
            } else {
                Room.databaseBuilder(context, HeroDB::class.java, HERO_TEST_DATABASE)
            }

            return dataBuilder.fallbackToDestructiveMigration().build()
        }
    }

    abstract fun heroDao(): HeroDao
    abstract fun heroRemoteKeysDao(): HeroRemoteKeysDao
}