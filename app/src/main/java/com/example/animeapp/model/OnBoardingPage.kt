package com.example.animeapp.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.example.animeapp.R

sealed class OnBoardingPage(
    @DrawableRes val image: Int,
    @StringRes val title: Int,
    @StringRes val description: Int
) {
    object First : OnBoardingPage(
        image = R.drawable.greetings,
        title = R.string.onboarding_greetings,
        description = R.string.onboarding_description
    )

    object Second : OnBoardingPage(
        image = R.drawable.explore,
        title = R.string.onboarding_explore,
        description = R.string.onboarding_description
    )

    object Third : OnBoardingPage(
        image = R.drawable.power,
        title = R.string.onboarding_power,
        description = R.string.onboarding_description
    )
}
