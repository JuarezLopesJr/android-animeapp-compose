package com.example.animeapp.navigation

import com.example.animeapp.utils.Constants.DETAILS_NAV_ARGUMENT_KEY
import com.example.animeapp.utils.Constants.DETAILS_SCREEN
import com.example.animeapp.utils.Constants.HOME_SCREEN
import com.example.animeapp.utils.Constants.SEARCH_SCREEN
import com.example.animeapp.utils.Constants.SPLASH_SCREEN
import com.example.animeapp.utils.Constants.WELCOME_SCREEN

sealed class Screen(val route: String) {
    object Splash : Screen(SPLASH_SCREEN)

    object Welcome : Screen(WELCOME_SCREEN)

    object Home : Screen(HOME_SCREEN)

    object Details : Screen("$DETAILS_SCREEN/{$DETAILS_NAV_ARGUMENT_KEY}") {
        fun passHeroId(heroId: Int) = "$DETAILS_SCREEN/$heroId"
    }

    object Search : Screen(SEARCH_SCREEN)
}
