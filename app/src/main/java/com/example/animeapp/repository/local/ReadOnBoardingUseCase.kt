package com.example.animeapp.repository.local

class ReadOnBoardingUseCase(
    private val repository: Repository
) {
    operator fun invoke() = repository.readOnBoardingState()
}