package com.example.animeapp.repository.local

class GetSelectedHeroUseCase(
    private val repository: Repository
) {
    suspend operator fun invoke(id: Int) =
        repository.getSelectedHeroRepository(id = id)
}