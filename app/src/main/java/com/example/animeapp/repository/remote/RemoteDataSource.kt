package com.example.animeapp.repository.remote

import androidx.paging.PagingData
import com.example.animeapp.model.Hero
import kotlinx.coroutines.flow.Flow

interface RemoteDataSource {
    fun getAllHeroesRemote(): Flow<PagingData<Hero>>

    fun searchHeroesRemote(query: String): Flow<PagingData<Hero>>
}