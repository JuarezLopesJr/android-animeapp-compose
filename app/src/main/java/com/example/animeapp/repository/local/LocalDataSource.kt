package com.example.animeapp.repository.local

import com.example.animeapp.model.Hero

interface LocalDataSource {
    suspend fun getSelectedHeroLocal(id: Int): Hero
}