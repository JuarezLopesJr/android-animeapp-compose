package com.example.animeapp.repository.local

import com.example.animeapp.model.Hero
import com.example.animeapp.model.HeroDB

class LocalDataSourceImpl(heroDB: HeroDB) : LocalDataSource {
    private val heroDao = heroDB.heroDao()

    override suspend fun getSelectedHeroLocal(id: Int): Hero {
        return heroDao.getSelectedHeroDao(heroId = id)
    }
}