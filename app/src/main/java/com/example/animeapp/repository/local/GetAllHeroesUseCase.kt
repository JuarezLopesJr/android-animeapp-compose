package com.example.animeapp.repository.local

class GetAllHeroesUseCase(
    private val repository: Repository
) {
    operator fun invoke() = repository.getAllHeroesRepository()
}