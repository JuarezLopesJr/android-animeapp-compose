package com.example.animeapp.repository.remote

import com.example.animeapp.model.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface AnimeApi {
    @GET("/boruto/heroes")
    suspend fun getAllHeroesApi(
        @Query("page") page: Int = 1
    ): ApiResponse

    @GET("/boruto/heroes/search")
    suspend fun searchHeroesApi(
        @Query("name") name: String
    ): ApiResponse
}