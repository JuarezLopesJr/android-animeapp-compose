package com.example.animeapp.repository.local

import androidx.paging.PagingData
import com.example.animeapp.model.Hero
import com.example.animeapp.repository.remote.RemoteDataSource
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow

class Repository @Inject constructor(
    private val local: LocalDataSource,
    private val remote: RemoteDataSource,
    private val dataStore: DataStoreOperations
) {

    fun getAllHeroesRepository(): Flow<PagingData<Hero>> {
        return remote.getAllHeroesRemote()
    }

    fun searchHeroesRepository(query: String): Flow<PagingData<Hero>> {
        return remote.searchHeroesRemote(query = query)
    }

    suspend fun getSelectedHeroRepository(id: Int): Hero {
        return local.getSelectedHeroLocal(id = id)
    }

    suspend fun saveOnBoardingState(completed: Boolean) {
        dataStore.saveOnBoardingStateRepository(completed)
    }

    fun readOnBoardingState() = dataStore.readOnBoardingStateRepository()
}