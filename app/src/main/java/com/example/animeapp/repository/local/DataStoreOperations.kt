package com.example.animeapp.repository.local

import kotlinx.coroutines.flow.Flow

interface DataStoreOperations {
    suspend fun saveOnBoardingStateRepository(completed: Boolean)

    fun readOnBoardingStateRepository(): Flow<Boolean>
}