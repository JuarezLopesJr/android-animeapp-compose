package com.example.animeapp.repository.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.animeapp.model.Hero
import com.example.animeapp.repository.remote.AnimeApi
import javax.inject.Inject

class SearchHeroesSource @Inject constructor(
    private val animeApi: AnimeApi,
    private val query: String
) : PagingSource<Int, Hero>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Hero> {
        return try {
            val apiResponse = animeApi.searchHeroesApi(name = query)
            val heroes = apiResponse.heroes

            if (heroes.isNotEmpty()) {
                LoadResult.Page(
                    data = heroes,
                    prevKey = apiResponse.prevPage,
                    nextKey = apiResponse.nextPage
                )
            } else {
                LoadResult.Page(
                    data = emptyList(),
                    prevKey = null,
                    nextKey = null
                )
            }
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Hero>): Int? {
        return state.anchorPosition
    }
}