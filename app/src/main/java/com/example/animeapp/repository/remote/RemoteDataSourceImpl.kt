package com.example.animeapp.repository.remote

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.animeapp.model.Hero
import com.example.animeapp.model.HeroDB
import com.example.animeapp.repository.paging.HeroRemoteMediator
import com.example.animeapp.repository.paging.SearchHeroesSource
import com.example.animeapp.utils.Constants.ITEMS_PER_PAGE
import kotlinx.coroutines.flow.Flow

@ExperimentalPagingApi
/* this parameters are not injected because its implementation is already set in
    NetworkModule (provideRemoteDataSource)
*/
class RemoteDataSourceImpl(
    private val animeApi: AnimeApi,
    private val heroDB: HeroDB
) : RemoteDataSource {
    private val heroDao = heroDB.heroDao()

    override fun getAllHeroesRemote(): Flow<PagingData<Hero>> {
        val pagingSourceFactory = { heroDao.getAllHeroesDao() }

        return Pager(
            config = PagingConfig(pageSize = ITEMS_PER_PAGE),
            remoteMediator = HeroRemoteMediator(
                animeApi = animeApi,
                heroDB = heroDB
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    override fun searchHeroesRemote(query: String): Flow<PagingData<Hero>> {
        return Pager(
            config = PagingConfig(pageSize = ITEMS_PER_PAGE),
            pagingSourceFactory = {
                SearchHeroesSource(animeApi = animeApi, query = query)
            }
        ).flow
    }
}