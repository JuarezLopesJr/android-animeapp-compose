package com.example.animeapp.repository.local

class SaveOnBoardingUseCase(
    private val repository: Repository
) {
     suspend operator fun invoke(completed: Boolean) {
        repository.saveOnBoardingState(completed)
    }
}