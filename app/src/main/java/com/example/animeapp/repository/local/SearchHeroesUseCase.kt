package com.example.animeapp.repository.local

class SearchHeroesUseCase(
    private val repository: Repository
) {
    operator fun invoke(query: String) =
        repository.searchHeroesRepository(query = query)
}