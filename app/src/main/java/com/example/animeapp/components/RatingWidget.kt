package com.example.animeapp.components

import android.util.Log
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.clipPath
import androidx.compose.ui.graphics.drawscope.scale
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.vector.PathParser
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.animeapp.R
import com.example.animeapp.ui.theme.EXTRA_SMALL_PADDING
import com.example.animeapp.ui.theme.LightGray
import com.example.animeapp.ui.theme.StarColor
import com.example.animeapp.utils.Constants.EMPTY_STARS
import com.example.animeapp.utils.Constants.FILLED_STARS
import com.example.animeapp.utils.Constants.HALF_FILLED_STARS

@Composable
fun RatingWidget(
    modifier: Modifier = Modifier,
    rating: Double,
    scaleFactor: Float = 3f,
    spaceBetween: Dp = EXTRA_SMALL_PADDING
) {
    val result = calculateStars(rating = rating)

    val starPathString = stringResource(id = R.string.star_path)

    val starPath = remember { PathParser().parsePathString(starPathString).toPath() }

    val starPathBounds = remember { starPath.getBounds() }

    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(spaceBetween)
    ) {
        result[FILLED_STARS]?.let {
            repeat(it) {
                FilledStar(
                    starPath = starPath,
                    starPathBounds = starPathBounds,
                    scaleFactor = scaleFactor
                )
            }
        }

        result[HALF_FILLED_STARS]?.let {
            repeat(it) {
                HalfFilledStar(
                    starPath = starPath,
                    starPathBounds = starPathBounds,
                    scaleFactor = scaleFactor
                )
            }
        }

        result[EMPTY_STARS]?.let {
            repeat(it) {
                EmptyStar(
                    starPath = starPath,
                    starPathBounds = starPathBounds,
                    scaleFactor = scaleFactor
                )
            }
        }
    }
}

@Composable
fun FilledStar(
    starPath: Path,
    starPathBounds: Rect,
    scaleFactor: Float
) {
    val filledStarDescription = stringResource(R.string.filled_star_description)

    Canvas(
        modifier = Modifier
            .size(24.dp)
            .semantics {
                contentDescription = filledStarDescription
            }
    ) {
        val canvasSize = size

        /* Controlling the size of the star by changing scaleFactor */
        scale(scale = scaleFactor) {
            val pathWidth = starPathBounds.width
            val pathHeight = starPathBounds.height

            /* Calculation to center the star (rectangle) in the canvas */
            val left = (canvasSize.width / 2f) - (pathWidth / 1.7f)
            val top = (canvasSize.height / 2f) - (pathHeight / 1.7f)

            translate(left = left, top = top) {
                drawPath(
                    path = starPath,
                    color = StarColor
                )
            }
        }
    }
}

@Composable
fun HalfFilledStar(
    starPath: Path,
    starPathBounds: Rect,
    scaleFactor: Float
) {
    val halfFilledStarDescription = stringResource(R.string.half_filled_star_description)

    Canvas(modifier = Modifier
        .size(24.dp)
        .semantics {
            contentDescription = halfFilledStarDescription
        }) {
        val canvasSize = size

        scale(scale = scaleFactor) {
            val pathWidth = starPathBounds.width
            val pathHeight = starPathBounds.height

            val left = (canvasSize.width / 2f) - (pathWidth / 1.7f)
            val top = (canvasSize.height / 2f) - (pathHeight / 1.7f)

            translate(left = left, top = top) {
                drawPath(
                    path = starPath,
                    color = LightGray.copy(alpha = 0.5f)
                )
                /* height must be aware of the scaleFactor, which is dynamic
                   clipPath() get the half of the star */
                clipPath(path = starPath) {
                    drawRect(
                        color = StarColor,
                        size = Size(
                            width = starPathBounds.maxDimension / 1.7f,
                            height = starPathBounds.maxDimension * scaleFactor
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun EmptyStar(
    starPath: Path,
    starPathBounds: Rect,
    scaleFactor: Float
) {
    val emptyFilledStarDescription = stringResource(R.string.empty_filled_star_description)

    Canvas(modifier = Modifier
        .size(24.dp)
        .semantics {
            contentDescription = emptyFilledStarDescription
        }) {
        val canvasSize = size

        scale(scale = scaleFactor) {
            val pathWidth = starPathBounds.width
            val pathHeight = starPathBounds.height

            val left = (canvasSize.width / 2f) - (pathWidth / 1.7f)
            val top = (canvasSize.height / 2f) - (pathHeight / 1.7f)

            translate(left = left, top = top) {
                drawPath(
                    path = starPath,
                    color = LightGray.copy(alpha = 0.5f)
                )
            }
        }
    }
}

@Composable
fun calculateStars(rating: Double): Map<String, Int> {
    val maxStars by remember { mutableStateOf(5) }

    var filledStars by remember { mutableStateOf(0) }

    var halfFilledStars by remember { mutableStateOf(0) }

    var emptyStars by remember { mutableStateOf(0) }

    LaunchedEffect(key1 = rating) {
        val (firstNumber, lastNumber) = rating.toString()
            .split(".")
            .map { it.toInt() }

        if (firstNumber in 0..5 && lastNumber in 0..9) {
            filledStars = firstNumber

            when (lastNumber) {
                in 1..5 -> halfFilledStars++
                in 6..9 -> filledStars++
            }

            if (firstNumber == 5 && lastNumber > 0) {
                emptyStars = 5
                filledStars = 0
                halfFilledStars = 0
            }
        } else {
            Log.d("Rating widget", "Invalid rating value...how did you get here ?!")
        }
    }

    emptyStars = maxStars - (filledStars + halfFilledStars)

    return mapOf(
        FILLED_STARS to filledStars,
        HALF_FILLED_STARS to halfFilledStars,
        EMPTY_STARS to emptyStars
    )
}

@Preview
@Composable
fun RatingPreview() {
    RatingWidget(rating = 0.0)
}

@Preview
@Composable
fun HalfStarPreview() {
    val starPathString = stringResource(id = R.string.star_path)

    val starPath = remember {
        PathParser().parsePathString(starPathString).toPath()
    }

    val starPathBounds = remember {
        starPath.getBounds()
    }

    HalfFilledStar(
        starPath = starPath,
        starPathBounds = starPathBounds,
        scaleFactor = 2f
    )
}

@Preview
@Composable
fun EmptyStarPreview() {
    val starPathString = stringResource(id = R.string.star_path)

    val starPath = remember {
        PathParser().parsePathString(starPathString).toPath()
    }

    val starPathBounds = remember {
        starPath.getBounds()
    }

    EmptyStar(
        starPath = starPath,
        starPathBounds = starPathBounds,
        scaleFactor = 2f
    )
}