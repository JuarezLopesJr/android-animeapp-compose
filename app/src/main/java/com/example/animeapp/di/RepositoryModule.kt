package com.example.animeapp.di

import android.content.Context
import com.example.animeapp.repository.local.DataStoreOperations
import com.example.animeapp.repository.local.DataStoreOperationsImpl
import com.example.animeapp.repository.local.GetAllHeroesUseCase
import com.example.animeapp.repository.local.GetSelectedHeroUseCase
import com.example.animeapp.repository.local.ReadOnBoardingUseCase
import com.example.animeapp.repository.local.Repository
import com.example.animeapp.repository.local.SaveOnBoardingUseCase
import com.example.animeapp.repository.local.SearchHeroesUseCase
import com.example.animeapp.repository.local.UseCases
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideDataStoreOperations(
        @ApplicationContext context: Context
    ): DataStoreOperations {
        return DataStoreOperationsImpl(context)
    }

    @Provides
    @Singleton
    fun provideUseCases(repository: Repository): UseCases {
        return UseCases(
            saveOnBoardingUseCase = SaveOnBoardingUseCase(repository),
            readOnBoardingUseCase = ReadOnBoardingUseCase(repository),
            getAllHeroesUseCase = GetAllHeroesUseCase(repository),
            searchHeroesUseCase = SearchHeroesUseCase(repository),
            getSelectedHeroUseCase = GetSelectedHeroUseCase(repository)
        )
    }
}