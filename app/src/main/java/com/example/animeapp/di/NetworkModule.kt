package com.example.animeapp.di

import androidx.paging.ExperimentalPagingApi
import com.example.animeapp.model.HeroDB
import com.example.animeapp.repository.remote.AnimeApi
import com.example.animeapp.repository.remote.RemoteDataSource
import com.example.animeapp.repository.remote.RemoteDataSourceImpl
import com.example.animeapp.utils.Constants.BASE_URL
import com.example.animeapp.utils.Constants.MEDIA_CONTENT_TYPE
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit

@ExperimentalPagingApi
@ExperimentalSerializationApi
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitInstance(okHttpClient: OkHttpClient): Retrofit {
        val contentType = MediaType.get(MEDIA_CONTENT_TYPE)
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(Json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    @Singleton
    fun provideAnimeApi(retrofit: Retrofit): AnimeApi {
        return retrofit.create(AnimeApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteDataSource(
        animeApi: AnimeApi,
        heroDB: HeroDB
    ): RemoteDataSource {
        return RemoteDataSourceImpl(animeApi = animeApi, heroDB = heroDB)
    }
}