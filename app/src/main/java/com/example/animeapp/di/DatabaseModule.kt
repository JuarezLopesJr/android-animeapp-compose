package com.example.animeapp.di

import android.content.Context
import androidx.room.Room
import com.example.animeapp.model.HeroDB
import com.example.animeapp.repository.local.LocalDataSource
import com.example.animeapp.repository.local.LocalDataSourceImpl
import com.example.animeapp.utils.Constants.HERO_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        HeroDB::class.java,
        HERO_DATABASE
    ).build()

    @Provides
    @Singleton
    fun provideLocalDataSource(heroDB: HeroDB): LocalDataSource {
        return LocalDataSourceImpl(heroDB = heroDB)
    }
}