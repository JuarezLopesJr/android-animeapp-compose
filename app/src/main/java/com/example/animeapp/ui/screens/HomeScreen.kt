package com.example.animeapp.ui.screens

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.paging.compose.collectAsLazyPagingItems
import com.example.animeapp.R
import com.example.animeapp.navigation.Screen
import com.example.animeapp.repository.presentation.ListContent
import com.example.animeapp.ui.theme.statusBarColor
import com.example.animeapp.ui.theme.topAppBarBackgroundColor
import com.example.animeapp.ui.theme.topAppBarContentColor
import com.example.animeapp.viewmodel.HomeViewModel
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@Composable
fun HomeScreen(
    homeViewModel: HomeViewModel = hiltViewModel(),
    navController: NavHostController
) {
    val allHeroes = homeViewModel.getAllHeroes.collectAsLazyPagingItems()

    val systemUiController = rememberSystemUiController()

    /* returning the status bar to its original color after set the color from the
     image color palette in DetailsScreen */
    systemUiController.setStatusBarColor(
        color = MaterialTheme.colors.statusBarColor
    )

    Scaffold(
        topBar = {
            HomeTopBar(
                onSearchClicked = {
                    navController.navigate(Screen.Search.route)
                }
            )
        },
        content = {
            ListContent(heroes = allHeroes, navController = navController)
        }
    )
}

@Composable
fun HomeTopBar(onSearchClicked: () -> Unit = {}) {
    TopAppBar(
        title = {
            Text(
                text = stringResource(R.string.top_bar_title),
                color = MaterialTheme.colors.topAppBarContentColor
            )
        },
        backgroundColor = MaterialTheme.colors.topAppBarBackgroundColor,
        actions = {
            IconButton(onClick = onSearchClicked) {
                Icon(
                    imageVector = Icons.Default.Search,
                    contentDescription = stringResource(R.string.search_icon_description)
                )
            }
        }
    )
}