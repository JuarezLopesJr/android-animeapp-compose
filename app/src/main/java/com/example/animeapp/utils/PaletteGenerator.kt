package com.example.animeapp.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.palette.graphics.Palette
import coil.ImageLoader
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.example.animeapp.utils.Constants.BLACK_COLOR
import com.example.animeapp.utils.Constants.DARK_VIBRANT
import com.example.animeapp.utils.Constants.ON_DARK_VIBRANT
import com.example.animeapp.utils.Constants.VIBRANT
import com.example.animeapp.utils.Constants.WHITE_COLOR

object PaletteGenerator {

    private fun parseColorSwatch(color: Palette.Swatch?): String {
        return if (color != null) {
            val parsedColor = Integer.toHexString(color.rgb)
            "#$parsedColor"
        } else {
            BLACK_COLOR
        }
    }

    private fun parseBodyColor(color: Int?): String {
        return if (color != null) {
            val parsedColor = Integer.toHexString(color)
            "#$parsedColor"
        } else {
            WHITE_COLOR
        }
    }

    suspend fun convertImageUrlToBitmap(
        imageUrl: String,
        context: Context
    ): Bitmap? {
        val loader = ImageLoader(context = context)

        val request = ImageRequest.Builder(context = context)
            .data(data = imageUrl)
            .allowHardware(false)
            .build()

        val imageResult = loader.execute(request = request)

        return if (imageResult is SuccessResult) {
            (imageResult.drawable as BitmapDrawable).bitmap
        } else {
            null
        }
    }

    fun extractColorsFromBitmap(bitmap: Bitmap): Map<String, String> {
        return mapOf(
            VIBRANT to parseColorSwatch(
                Palette.from(bitmap).generate().vibrantSwatch
            ),

            DARK_VIBRANT to parseColorSwatch(
                Palette.from(bitmap).generate().darkVibrantSwatch
            ),

            ON_DARK_VIBRANT to parseBodyColor(
                Palette.from(bitmap).generate().darkVibrantSwatch?.bodyTextColor
            )
        )
    }
}