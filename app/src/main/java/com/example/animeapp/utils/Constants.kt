package com.example.animeapp.utils


object Constants {
    // Routes
    const val SPLASH_SCREEN = "splash_screen"
    const val WELCOME_SCREEN = "welcome_screen"
    const val HOME_SCREEN = "home_screen"
    const val DETAILS_SCREEN = "details_screen"
    const val SEARCH_SCREEN = "search_screen"
    const val DETAILS_NAV_ARGUMENT_KEY = "heroId"

    // Room
    const val HERO_DATABASE = "hero_database"
    const val HERO_DATABASE_TABLE = "hero_table"
    const val HERO_REMOTE_KEYS_DATABASE_TABLE = "hero_remote_keys_table"
    const val HERO_TEST_DATABASE = "test_database.db"

    // Paging
    const val ON_BOARDING_PAGE_COUNT = 3
    const val LAST_ON_BOARDING_PAGE = 2
    const val ITEMS_PER_PAGE = 3
    const val ABOUT_TEXT_MAX_LINES = 7

    // DataStore
    const val PREFERENCES_NAME = "anime_app_preferences"
    const val PREFERENCES_KEY = "on_boarding_completed"

    // Retrofit
    const val BASE_URL = "http://10.0.2.2:8080" // Android emulator default localhost
    const val MEDIA_CONTENT_TYPE = "application/json"

    // Rating fields
    const val FILLED_STARS = "filledStars"
    const val HALF_FILLED_STARS = "halfFilledStars"
    const val EMPTY_STARS = "emptyStars"

    // Error exceptions
    const val SOCKET_EXCEPTION_MESSAGE = "Server Unavailable"
    const val CONNECTION_EXCEPTION_MESSAGE = "Internet Unavailable"
    const val UNKNOWN_ERROR = "Unknown error"
    const val DETAILS_VIEWMODEL_TAG = "DetailsViewModel"

    // Images
    const val MIN_BACKGROUND_IMAGE_HEIGHT = 0.4f

    // Palette
    const val BLACK_COLOR = "#000000"
    const val WHITE_COLOR = "#FFFFFF"
    const val VIBRANT = "vibrant"
    const val DARK_VIBRANT = "darkVibrant"
    const val ON_DARK_VIBRANT = "onDarkVibrant"
}

