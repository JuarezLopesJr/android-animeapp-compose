package com.example.animeapp.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingConfig
import androidx.paging.PagingState
import androidx.paging.RemoteMediator.MediatorResult.Error
import androidx.paging.RemoteMediator.MediatorResult.Success
import androidx.test.core.app.ApplicationProvider
import com.example.animeapp.model.Hero
import com.example.animeapp.model.HeroDB
import com.example.animeapp.remote.MockAnimeApi2
import com.example.animeapp.repository.paging.HeroRemoteMediator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

@ExperimentalPagingApi
@ExperimentalCoroutinesApi
class HeroRemoteMediatorTests {

    private lateinit var animeApi: MockAnimeApi2
    private lateinit var heroDB: HeroDB

    @Before
    fun setupTests() {
        animeApi = MockAnimeApi2()
        heroDB = HeroDB.create(
            context = ApplicationProvider.getApplicationContext(),
            useInMemory = true
        )
    }

    @After
    fun cleanupTests() {
        heroDB.clearAllTables()
    }

    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() =
        runBlocking {
            val remoteMediator = HeroRemoteMediator(
                animeApi = animeApi,
                heroDB = heroDB
            )

            val pagingState = PagingState<Int, Hero>(
                pages = emptyList(),
                anchorPosition = null,
                config = PagingConfig(pageSize = 3),
                leadingPlaceholderCount = 0
            )

            val result =
                remoteMediator.load(LoadType.REFRESH, pagingState)

            assertTrue(result is Success)

            assertFalse((result as Success).endOfPaginationReached)
        }

    @Test
    fun refreshLoadSuccessAndEndOfPaginationTrueWhenNoMoreData() =
        runBlocking {
            animeApi.clearData()

            val remoteMediator = HeroRemoteMediator(
                animeApi = animeApi,
                heroDB = heroDB
            )

            val pagingState = PagingState<Int, Hero>(
                pages = emptyList(),
                anchorPosition = null,
                config = PagingConfig(pageSize = 3),
                leadingPlaceholderCount = 0
            )

            val result =
                remoteMediator.load(LoadType.REFRESH, pagingState)

            assertTrue(result is Success)

            assertTrue((result as Success).endOfPaginationReached)
        }

    @Test
    fun refreshLoadReturnsErrorResultWhenErrorOccurs() =
        runBlocking {
            animeApi.addException()

            val remoteMediator = HeroRemoteMediator(
                animeApi = animeApi,
                heroDB = heroDB
            )

            val pagingState = PagingState<Int, Hero>(
                pages = emptyList(),
                anchorPosition = null,
                config = PagingConfig(pageSize = 3),
                leadingPlaceholderCount = 0
            )

            val result =
                remoteMediator.load(LoadType.REFRESH, pagingState)

            assertTrue(result is Error)

        }
}