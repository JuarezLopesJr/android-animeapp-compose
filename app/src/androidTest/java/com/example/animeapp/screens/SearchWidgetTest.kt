package com.example.animeapp.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.assertTextContains
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import com.example.animeapp.R
import com.example.animeapp.ui.screens.SearchWidget
import org.junit.Rule
import org.junit.Test

class SearchWidgetTest {
    private lateinit var textFieldDescription: String
    private lateinit var closeIconDescription: String
    private lateinit var searchDescription: String

    @get:Rule
    val composeTestRule = createComposeRule()

    @ExperimentalComposeUiApi
    @Test
    fun openSearchWidget_addInputText_assertInputText() {
        var text by mutableStateOf("")

        composeTestRule.setContent {
            SearchWidget(
                text = text,
                onTextChange = { text = it },
                onSearchClicked = {},
                onCloseClicked = {}
            )

            textFieldDescription = stringResource(R.string.text_field_description)
        }

        composeTestRule.apply {

            /* finding component and performing action (input text) */
            onNodeWithContentDescription(textFieldDescription)
                .performTextInput("Mussum ipsum")

            /* asserting that selected component has the same text input */
            onNodeWithContentDescription(textFieldDescription)
                .assertTextEquals("Mussum ipsum")
        }
    }

    @ExperimentalComposeUiApi
    @Test
    fun openSearchWidget_addInputText_pressCloseButtonOnce_assertEmptyInputText() {
        var text by mutableStateOf("")

        composeTestRule.setContent {
            SearchWidget(
                text = text,
                onTextChange = { text = it },
                onSearchClicked = {},
                onCloseClicked = {}
            )

            textFieldDescription = stringResource(R.string.text_field_description)
            closeIconDescription = stringResource(R.string.close_icon_description)
        }

        composeTestRule.apply {

            /* finding component and performing action (input text) */
            onNodeWithContentDescription(textFieldDescription)
                .performTextInput("Mussum ipsum")

            /* performing click action */
            onNodeWithContentDescription(closeIconDescription)
                .performClick()

            /* asserting that selected component has the same text input */
            onNodeWithContentDescription(textFieldDescription)
                .assertTextContains("")
        }
    }

    @ExperimentalComposeUiApi
    @Test
    fun openSearchWidget_addInputText_pressCloseButtonTwice_assertClosedState() {
        var text by mutableStateOf("")
        var searchWidgetShown by mutableStateOf(true)

        composeTestRule.setContent {
            /* condition to verify if the component will be closed */
            if (searchWidgetShown) {
                SearchWidget(
                    text = text,
                    onTextChange = { text = it },
                    onSearchClicked = {},
                    onCloseClicked = { searchWidgetShown = false }
                )
            }

            textFieldDescription = stringResource(R.string.text_field_description)
            closeIconDescription = stringResource(R.string.close_icon_description)
            searchDescription = stringResource(id = R.string.search_widget_description)
        }

        composeTestRule.apply {

            /* finding component and performing action (input text) */
            onNodeWithContentDescription(textFieldDescription)
                .performTextInput("Mussum ipsum")

            /* performing click action twice */
            onNodeWithContentDescription(closeIconDescription)
                .performClick()

            /* asserting that selected component has empty text input */
            onNodeWithContentDescription(textFieldDescription)
                .assertTextContains("")

            onNodeWithContentDescription(closeIconDescription)
                .performClick()

            onNodeWithContentDescription(searchDescription)
                .assertDoesNotExist()
        }
    }

    @ExperimentalComposeUiApi
    @Test
    fun openSearchWidget_pressCloseButtonOnceWhenEmpty_assertClosedState() {
        val text = ""
        var searchWidgetShown by mutableStateOf(true)

        composeTestRule.setContent {
            /* condition to verify if the component will be closed */
            if (searchWidgetShown) {
                SearchWidget(
                    text = text,
                    onTextChange = {},
                    onSearchClicked = {},
                    onCloseClicked = { searchWidgetShown = false }
                )
            }

            closeIconDescription = stringResource(R.string.close_icon_description)
            searchDescription = stringResource(id = R.string.search_widget_description)
        }

        composeTestRule.apply {

            /* performing click action */
            onNodeWithContentDescription(closeIconDescription)
                .performClick()

            onNodeWithContentDescription(searchDescription)
                .assertDoesNotExist()
        }
    }
}