package com.example.animeapp.components

import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithContentDescription
import com.example.animeapp.R
import com.example.animeapp.ui.theme.SMALL_PADDING
import org.junit.Rule
import org.junit.Test

class RatingWidgetTest {
    private lateinit var filledStarDescription: String
    private lateinit var halfStarDescription: String
    private lateinit var emptyStarDescription: String

    @get:Rule
    val composeTestRule = createComposeRule()

    private fun setupTests(value: Double) {
        composeTestRule.setContent {
            RatingWidget(
                modifier = Modifier.padding(all = SMALL_PADDING),
                rating = value
            )

            emptyStarDescription = stringResource(R.string.empty_filled_star_description)
            halfStarDescription = stringResource(R.string.half_filled_star_description)
            filledStarDescription = stringResource(R.string.filled_star_description)
        }
    }

    @Test
    fun passZeroPointZeroValue_Assert_FiveEmptyStars() {
        setupTests(0.0)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(5)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(0)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(0)
        }
    }

    @Test
    fun passZeroPointFiveValue_Assert_FourEmpty_OneHalf_And_ZeroFilledStars() {
        setupTests(0.5)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(4)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(1)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(0)
        }
    }

    @Test
    fun passZeroPointSixValue_Assert_FourEmpty_ZeroHalf_And_OneFilledStar() {
        setupTests(0.6)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(4)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(0)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(1)
        }
    }

    @Test
    fun passFourPointZeroValue_Assert_OneEmpty_ZeroHalf_And_FourFilledStar() {
        setupTests(4.0)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(1)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(0)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(4)
        }
    }

    @Test
    fun passFourPointThreeValue_Assert_ZeroEmpty_OneHalf_And_FourFilledStar() {
        setupTests(4.3)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(0)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(1)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(4)
        }
    }

    @Test
    fun passNegativeValue_Assert_FiveEmptyStar() {
        setupTests(-1.0)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(5)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(0)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(0)
        }
    }

    @Test
    fun passInvalidValue_Assert_FiveEmptyStar() {
        setupTests(5.1)

        composeTestRule.apply {
            onAllNodesWithContentDescription(emptyStarDescription)
                .assertCountEquals(5)

            onAllNodesWithContentDescription(halfStarDescription)
                .assertCountEquals(0)

            onAllNodesWithContentDescription(filledStarDescription)
                .assertCountEquals(0)
        }
    }
}