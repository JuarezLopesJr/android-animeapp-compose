package com.example.animeapp.paging

import androidx.paging.PagingSource.LoadParams.Refresh
import androidx.paging.PagingSource.LoadResult
import com.example.animeapp.model.Hero
import com.example.animeapp.remote.MockAnimeApi
import com.example.animeapp.repository.paging.SearchHeroesSource
import com.example.animeapp.repository.remote.AnimeApi
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class SearchHeroesSourceTest {
    private lateinit var animeApi: AnimeApi
    private lateinit var heroes: List<Hero>

    @Before
    fun setupTests() {
        animeApi = MockAnimeApi()
        heroes = listOf(
            Hero(
                id = 1,
                name = "Sasuke",
                image = "",
                about = "",
                rating = 5.0,
                power = 0,
                month = "",
                day = "",
                family = listOf(),
                abilities = listOf(),
                natureTypes = listOf()
            ),
            Hero(
                id = 2,
                name = "Naruto",
                image = "",
                about = "",
                rating = 5.0,
                power = 0,
                month = "",
                day = "",
                family = listOf(),
                abilities = listOf(),
                natureTypes = listOf()
            ),
            Hero(
                id = 3,
                name = "Sakura",
                image = "",
                about = "",
                rating = 5.0,
                power = 0,
                month = "",
                day = "",
                family = listOf(),
                abilities = listOf(),
                natureTypes = listOf()
            )
        )
    }

    @Test
    fun `search api with existing hero name, expect single hero result, assert LoadingResult_Page`() {
        runBlockingTest {
            val heroSource = SearchHeroesSource(animeApi, "Sasuke")

            assertEquals<LoadResult<Int, Hero>>(
                expected = LoadResult.Page(
                    data = listOf(heroes.first()),
                    prevKey = null,
                    nextKey = null
                ),
                actual = heroSource.load(
                    Refresh(
                        key = null,
                        loadSize = 3,
                        placeholdersEnabled = false
                    )
                )
            )
        }
    }

    @Test
    fun `search api with existing hero name, expect multiple hero result, assert LoadingResult_Page`() {
        runBlockingTest {
            val heroSource = SearchHeroesSource(animeApi, "Sa")

            assertEquals<LoadResult<Int, Hero>>(
                expected = LoadResult.Page(
                    data = listOf(heroes.first(), heroes[2]),
                    prevKey = null,
                    nextKey = null
                ),
                actual = heroSource.load(
                    Refresh(
                        key = null,
                        loadSize = 3,
                        placeholdersEnabled = false
                    )
                )
            )
        }
    }

    @Test
    fun `search api with empty hero name, assert empty list LoadingResult_Page`() {
        runBlockingTest {
            val heroSource = SearchHeroesSource(animeApi, "")

            val loadResult = heroSource.load(
                Refresh(
                    key = null,
                    loadSize = 3,
                    placeholdersEnabled = false
                )
            )

            val result = animeApi.searchHeroesApi("").heroes

            assertTrue { result.isEmpty() }

            assertTrue { loadResult is LoadResult.Page }

        }
    }

    @Test
    fun `search api with non-existing hero name, assert empty list LoadingResult_Page`() {
        runBlockingTest {
            val heroSource = SearchHeroesSource(animeApi, "unknown")

            val loadResult = heroSource.load(
                Refresh(
                    key = null,
                    loadSize = 3,
                    placeholdersEnabled = false
                )
            )

            val result = animeApi.searchHeroesApi("unknown").heroes

            assertTrue { result.isEmpty() }

            assertTrue { loadResult is LoadResult.Page }
        }
    }
}